﻿import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './home';
import { LoginComponent } from './login';
import { AuthGuard } from './_guards';
import { PostComponent } from './post/post.component';
import { CommentComponent } from './comment/comment.component';
import { SignupComponent } from './signup/signup.component';

const appRoutes: Routes = [
    { path: '', component: PostComponent },
    { path: 'login', component: LoginComponent },
    { path: 'signup', component: SignupComponent},
    // otherwise redirect to home
    { path: '**', redirectTo: '' }
];

export const routing = RouterModule.forRoot(appRoutes);