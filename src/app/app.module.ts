﻿import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { ReactiveFormsModule }    from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

// used to create fake backend
import { fakeBackendProvider } from './_helpers';

import { AppComponent }  from './app.component';
import { routing }        from './app.routing';

import { AuthGuard } from './_guards';
import { JwtInterceptor } from './_helpers';
import { AuthenticationService, UserService } from './_services';
import { HomeComponent } from './home';
import { LoginComponent } from './login';;
import { PostComponent } from './post/post.component'
import { PostService } from './post/post.service';;
import { CommentComponent } from './comment/comment.component'
;
import { NavbarComponent } from './navbar/navbar.component'
;
import { SignupComponent } from './signup/signup.component'
import { CustomEventManager } from './_helpers/event-manager.service';;

@NgModule({
    imports: [
        BrowserModule,
        ReactiveFormsModule,
        HttpClientModule,
        routing
    ],
    declarations: [
        AppComponent,
        HomeComponent,
        LoginComponent
,
        PostComponent ,
        CommentComponent ,
        NavbarComponent ,
        SignupComponent],
    providers: [
        AuthGuard,
        AuthenticationService,
        UserService,
        {
            provide: HTTP_INTERCEPTORS,
            useClass: JwtInterceptor,
            multi: true
        },
        PostService,
        CustomEventManager,
        // provider used to create fake backend
        fakeBackendProvider
    ],
    bootstrap: [AppComponent]
})

export class AppModule { }