import { Component, OnInit } from '@angular/core';
import { CommentService } from './comment.service';

@Component({
  selector: 'app-comment',
  templateUrl: './comment.component.html',
  styleUrls: ['./comment.component.css']
})
export class CommentComponent implements OnInit {

  comment: Array<any>;
  
  constructor(private commentService: CommentService) { }

  ngOnInit() {
    this.commentService.getAll().subscribe(data =>{
      this.comment = data;
  });
  }

}
