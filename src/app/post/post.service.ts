import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PostService {

  private resourceUrl = '/api/post/user';

  constructor(private http: HttpClient) { }

  getAll(): Observable<any> {
    return this.http.get('/api/post');
  }

  loadByUser(user: string): Observable<any> {
    return this.http.get(`${this.resourceUrl}/${user}`);
  }

  savePost(title: string, postText: string, tag: string) {
    return this.http.post('/api/post', {title: title, postText: postText, tagString: tag})
    .pipe(map((res:any) => {
      console.log(res);
  }));
  }
  
}
