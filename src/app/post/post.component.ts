import { Component, OnInit, OnDestroy } from '@angular/core';
import { PostService } from './post.service';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CustomEventManager } from '../_helpers/event-manager.service';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css']
})
export class PostComponent implements OnInit {
  postForm: FormGroup;
  posts: Array<any>;
  comment: Array<any>;
  tags: Array<any>;
  error = '';


  constructor(private postService: PostService,
    private formBuilder: FormBuilder,
    private eventManager: CustomEventManager
) { }

  ngOnInit() {
    this.postForm = this.formBuilder.group({
      title: ['', Validators.required],
      postText: ['', Validators.required],
      tags: ['']
  });
    this.loadAll();
  }

  loadAll() {
    this.postService.getAll().subscribe(data =>{
      this.posts = data;
  });
  }

  loadByUser(name: string) {
    this.postService.loadByUser(name).subscribe(data =>{
      this.posts = data;
  });
  }

  get f() { return this.postForm.controls; }

  savePost() {
    if (this.postForm.invalid) {
      return;
  }
    this.postService.savePost(this.f.title.value, this.f.postText.value, this.f.tags.value).subscribe(
      data =>{
        this.ngOnInit();
        // this.postForm.clearValidators;
      },
      error =>{
        this.error = error;
      });
  }

  isAuthenticated() {
    let currentUser = JSON.parse(localStorage.getItem('currentUser'));
    if (currentUser && currentUser.token) {
        return true;
    }
    return false;
  }
}
